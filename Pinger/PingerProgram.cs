﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;
using System;

namespace Pinger
{
    class PingerProgram
    {

        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };

            MessageProducerScopeFactory messageProducerScope = new MessageProducerScopeFactory(factory);
            MessageConsumerScopeFactory messageConsumerScope = new MessageConsumerScopeFactory(factory);
            QueueService queueService = new QueueService(messageProducerScope, messageConsumerScope, "ping_queue", "pong_queue", "pong");


            Console.ReadLine();
        }

    }

}

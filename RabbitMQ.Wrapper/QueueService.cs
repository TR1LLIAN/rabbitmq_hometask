﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;
using System.Text;
using System.Threading;

namespace RabbitMQ.Wrapper
{
    public class QueueService
    {
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IMessageConsumerScope _messageConsumerScope;
        private readonly string _queName;

        public QueueService(IMessageProducerScopeFactory messageProducerScopeFactory,
                            IMessageConsumerScopeFactory messageConsumerScopeFactory, string ConsumerQueue, string CustomerQueue, string QueName)
        {
            _queName = QueName;
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "rabbit",
                ExchangeType = ExchangeType.Direct,
                QueueName = CustomerQueue,
                RoutingKey = "rabbit"
            });
            _messageConsumerScope = messageConsumerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "rabbit",
                ExchangeType = ExchangeType.Direct,
                QueueName = ConsumerQueue,
                RoutingKey = "rabbit"
            });

            _messageConsumerScope.MessageConsumer.Received += ListenToQueue;

        }

        private void ListenToQueue(object sender, Client.Events.BasicDeliverEventArgs e)
        {
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine("Received next message [ {0} ]", message + " " + DateTime.Now);
            Thread.Sleep(2500);
            SendMessageToQueue(DateTime.Now + " " + _queName);
        }

        public void SendMessageToQueue(string value)
        {
            Console.WriteLine($"Send message from ( {_queName} ) next text: [ {value} ]");
            _messageProducerScope.MessageProducer.Send(value);
            Console.WriteLine();
        }

 

        public void Dispose()
        {
            _messageConsumerScope.Dispose();
            _messageProducerScope.Dispose();
        }
    }
}

﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Text;
using System.Threading;

namespace Ponger
{
    class PongerProgram
    {
        static void Main(string[] args)
        {

            var factory = new ConnectionFactory() { HostName = "localhost" };

            MessageProducerScopeFactory messageProducerScope = new MessageProducerScopeFactory(factory);
            messageProducerScope.Open(new MessageScopeSettings { ExchangeName = "rabbit", ExchangeType = ExchangeType.Direct, QueueName = "pong_queue", RoutingKey = "rabbit" });
            MessageConsumerScopeFactory messageConsumerScope = new MessageConsumerScopeFactory(factory);
            var queueService = new QueueService(messageProducerScope, messageConsumerScope, "pong_queue", "ping_queue", "ping");
            queueService.SendMessageToQueue(DateTime.Now + " ping");

            Console.ReadLine();


        }
    }
}
